import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './ProductTile.css';

class ProductTile extends Component {
    render() {
        const { item, onAddToCart } = this.props;
        const imageStyle = {
            backgroundImage: 'url(/media/' + item.image + ')'
        }

        return (
            <div className="product-tile">
                <div className="product-tile-image" style={imageStyle}></div>
                <h4 className="product-tile-brand">{item.brand}</h4>
                <h3 className="product-tile-heading">{item.title}</h3>
                <span className="product-tile-price">{'$' + item.price + '.00'}</span>
                <div className="product-tile-cta">
                    <Link className="product-tile-view-details" to={'/product/' + item.id}>View details</Link>
                    <div onClick={() => onAddToCart(item.id, 1)} className="product-tile-add-cart">Add to cart</div>
                </div>
            </div>
        );
    }
}

export default ProductTile;
