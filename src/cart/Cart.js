import React, { Component } from 'react';
import './Cart.css';

class Cart extends Component {
    render() {
        const { cart, onChangeToCart } = this.props;
        const cartLength = cart.items.length > 0;

        return (

            <div className="Cart">
                <h2>Shopping Cart</h2>

                { cartLength ? (
                        <div className="cart-container">
                            <table className="cart-table">
                                <thead>
                                    <tr className="cart-product-row">
                                        <th className="cart-product-description">Product</th>
                                        <th className="cart-increment-container">Quantity</th>
                                        <th className="cart-price">Total</th>
                                        <th className="cart-remove">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    { cart && cart.items.map(product =>
                                        <tr key={product.id} className="cart-product-row">
                                            <td className="cart-product-description">
                                                <img className="cart-product-image" alt={product.title} src={'/media/' + product.image} />
                                                <p>{product.brand}</p>
                                                <p>{product.title}</p>
                                            </td>
                                            <td className="cart-increment-container">
                                                <div className="cart-increment-total">{product.numberOfItems}</div>
                                                <div>
                                                    <div onClick={() => onChangeToCart(product.id, 'increment')} className="cart-increment">+</div>
                                                    <div onClick={() => onChangeToCart(product.id, 'decrement')} className="cart-decrement">-</div>
                                                </div>
                                            </td>
                                            <td className="cart-price">
                                                {'$' + product.price + '.00'}
                                            </td>
                                            <td className="cart-remove">
                                                <span onClick={() => onChangeToCart(product.id, 'delete')}>x</span>
                                            </td>
                                        </tr>
                                    ) }

                                </tbody>
                            </table>
                            <div className="cart-overview">
                                Cart Overview
                                <div>
                                    Subtotal <span>{'$' + cart.total + '.00'}</span>
                                </div>
                                <div>
                                    Total <span>{'$' + cart.total + '.00 CAD'}</span>
                                </div>
                            </div>
                        </div>
                ) : (
                    <p>Please add something to your cart</p>
                )}

            </div>
        );
    }
}

export default Cart;
