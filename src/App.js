import React, { Component } from 'react';
import { Link, Route } from 'react-router-dom';
import { getProduct } from './helpers.js';
import './App.css';
import Category from "./category/Category";
import Cart from "./cart/Cart";
import MiniCart from "./miniCart/MiniCart";
import Product from "./product/Product";

class App extends Component {
    constructor() {
        super();
        this.state = {
            items: [],
            cart: {
                items: [],
                total: 0
            }
        };
    }

    componentDidMount() {
        fetch('/products.json')
            .then(res => res.json())
            .then(
                (result) => {
                    for (var i = 0; i < result.length; i++) {
                        result[i].id = result[i].title.toLowerCase().split(' ').join('-');
                    }

                    this.setState({
                        items: result
                    });
                },
                (error) => {
                    this.setState({
                        error
                    });
                }
            )
    }

    handleChangeToCart = (id, action) => {
        const tempCart = { ...this.state.cart };
        let currentItem;

        for (var i = 0; i < tempCart.items.length; i++) {
            currentItem = tempCart.items[i];

            if (currentItem.id === id) {

                switch(action) {
                    case 'increment':
                        currentItem.numberOfItems++;
                        tempCart.total += currentItem.price;
                        break;

                    case 'decrement':
                        if (currentItem.numberOfItems > 1) {
                            currentItem.numberOfItems--;
                        }

                        tempCart.total -= currentItem.price;
                        break;

                    case 'delete':
                        tempCart.items.splice(tempCart.items.indexOf(currentItem), 1);
                        tempCart.total -= (currentItem.price * currentItem.numberOfItems);
                        break;

                    default:
                        break;
                }
            }
        }

        this.setState({cart: tempCart});
    }

    handleAddToCart = (id, numberOfItems) => {
        const tempCart = { ...this.state.cart };
        let currentItem;

        for (var i = 0; i < tempCart.items.length; i++) {
            currentItem = tempCart.items[i];

            if (currentItem.id === id) {
                currentItem.numberOfItems = currentItem.numberOfItems + numberOfItems;
                tempCart.total += currentItem.price * numberOfItems;
                this.setState({cart: tempCart});
                return;
            }
        }

        const product = getProduct(id, this.state.items);
        product.numberOfItems = numberOfItems;
        delete product.description;
        tempCart.items.push(product);
        tempCart.total += product.price * numberOfItems;
        this.setState({ cart: tempCart });
    }

    render() {
        const { items, cart } = this.state;

        return (
            <div className="App">
                <header>
                    <div className="logo-container"><h1 className="logo">Hero</h1></div>
                    <nav>
                        <Link to="/">Home</Link>
                        <span>Shop<i className="fas fa-caret-down"></i></span>
                        <span>Journal</span>
                        <span>More<i className="fas fa-caret-down"></i></span>
                    </nav>
                    <Link className="mini-cart-label" to="/cart">My Cart ({cart.items.length})<i className="fas fa-caret-down"></i></Link>
                    <MiniCart cart={cart} onChangeToCart={this.handleChangeToCart} />
                </header>

                <Route
                    exact
                    path="/"
                    render={props => (
                        <Category {...props} items={items} onAddToCart={this.handleAddToCart} />
                    )}
                />
                <Route
                    path="/cart"
                    render={props => (
                        <Cart {...props} cart={cart} onChangeToCart={this.handleChangeToCart} />
                    )}
                />
                <Route
                    path="/product/:id"
                    render={props => (
                        <Product {...props} items={items} onAddToCart={this.handleAddToCart} />
                    )}
                />
            </div>
        );
    }
}

export default App;
