import React, { Component } from 'react';
import { getProduct } from '../helpers.js';
import './Product.css';

class Product extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: props.match.params.id,
            numberOfItems: 1
        };
    }

    render() {
        const product = (this.props.items && getProduct(this.state.id, this.props.items)) || {};
        const { id, numberOfItems } = this.state;

        return (
            <div className="Product">
                <span className="product-breadcrumbs">
                    Home / Plates / <span>{product.title}</span>
                </span>

                <div className="product-container">
                    <img alt={product.title} className="product-image" src={'/media/' + product.image} />

                    <div className="product-info">
                        <h4 className="product-brand">{product.brand}</h4>
                        <h3 className="product-heading">{product.title}</h3>
                        <span className="product-price">{'$' + product.price + '.00'}</span>
                        <p className="product-description">{product.description}</p>

                        <div className="product-add-cart-container">
                            <div className="product-cart-total">{numberOfItems}</div>
                            <div>
                                <div onClick={this.increment.bind(this)} className="product-increment">+</div>
                                <div onClick={this.decrement.bind(this)} className="product-decrement">-</div>
                            </div>
                            <div onClick={() => this.props.onAddToCart(id, numberOfItems)} className="product-cta">Add to cart</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    increment() {
        this.setState({
            numberOfItems: this.state.numberOfItems + 1
        });
    }

    decrement() {
        if (this.state.numberOfItems > 1) {
            this.setState({
                numberOfItems: this.state.numberOfItems - 1
            });
        }
    }
}

export default Product;
