import React, { Component } from 'react';
import ProductTile from "../productTile/ProductTile";
import './Category.css';

class Category extends Component {
    render() {
        return (
            <div className="Category">
                <div className="category-banner">
                    <div className="category-banner-copy">
                        <h2 className="category-heading">Plates</h2>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam ornare wisi eu metus. Maecenas fermentum, sem in pharetra pellentesque, velit turpis volutpat ante, in pharetra metus odio a lectus.</p>
                    </div>
                </div>

                <div className="category-body">
                    { this.props.items && this.props.items.map(item =>
                        <ProductTile
                            key={item.id}
                            item={item}
                            onAddToCart={this.props.onAddToCart}
                        />) }
                </div>
            </div>
        );
    }
}

export default Category;
