import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './MiniCart.css';

class MiniCart extends Component {
    render() {
        const { cart } = this.props;
        let showMinicart = (cart.items.length > 0 && window.location.pathname !== '/cart') ? ' show' : '';

        return (
            <div className={'MiniCart' + showMinicart}>
                <div>
                    <div>
                        { cart && cart.items.map(product =>
                            <div key={product.id} className="minicart-product">
                                <img src={'/media/' + product.image} alt={product.title} />
                                <span className="minicart-title">{product.title + ' x ' + product.numberOfItems}</span>
                                <span className="minicart-brand">{product.brand}</span>
                                <span className="minicart-price">{'$' + product.price + '.00'}</span>
                                <i onClick={() => this.props.onChangeToCart(product.id, 'delete')} className="fas fa-times"></i>
                            </div>
                        )}
                    </div>
                    <div className="minicart-cta-row">
                        <div className="minicart-total">Total <span>{'$' + cart.total + '.00'}</span></div>

                        <Link to="/cart" className="minicart-cta">
                            View cart
                        </Link>
                        <div className="minicart-cta">
                            Checkout
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default MiniCart;
